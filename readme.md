# Vorlage für ein Gründungsprotokoll

## PDF-Preview

[PREVIEW](https://gitlab.com/Felix-Ulonska/GruendungsvorlageTex/-/jobs/artifacts/main/file/main.pdf?job=build)

## Es wird keine Garantie für eine Richtigkeit dieser Vorlage gemacht

Ich bin ein juristischer Laie!

## How To

- Die Satzung ist stark inspiriert von folgender Vorlage: [Vereinswiki](https://www.vereinswiki.info/node/158)
- Auch zu empfehlen sind: [BMJV](https://www.bmjv.de/SharedDocs/Downloads/DE/Formulare/Muster_eines_Gruendungsprotokolls.pdf?__blob=publicationFile&v=2),  [Justiz Bayern](https://www.justiz.bayern.de/media/images/behoerden-und-gerichte/muster_gruendungsprotokoll.pdf)
- Die Stellen, die ersetz werden müssen sind per Math Modus gekennzeichnet

## Contributions sind willkommen!